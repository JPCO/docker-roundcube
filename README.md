# Roundcube
---
Made because I needed a simple roundcube latest version installation.  

I myself use environment variables for configuration:
---
    ROUNDCUBE_USERNAME_DOMAIN=example.loc
    ROUNDCUBE_DEFAULT_HOST=tls://dovecot
    ROUNDCUBE_SMTP_SERVER=tls://postfix
    ROUNDCUBE_PRODUCT_NAME='MyDomain - Webmail'

## Mapping
For Mapping these locations are used:
Volume: /var/mail:
    /var/mail/roundcube/sqlite.db
Volume: /var/www/html
    /var/www/html/roundcube


Nginx configuration example:
```
server {
    listen 443;

    server_name         mydomain.com;
    index               index.php;

    root                /var/www/html/roundcube;
    
    location ~ ^/.*\.php$ {
        include         fastcgi.conf;
        fastcgi_pass    roundcube:9000;
    }
}

```