#!/bin/sh
set -e

setenv.sh

if [ ! -e roundcube ]; then
	mkdir roundcube
else
	rm -r roundcube/*
fi
tar xf /usr/src/roundcube.tar.gz -C roundcube
chown -R www-data roundcube
exec "$@"